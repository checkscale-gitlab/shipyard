/*
 * Copyright (C) 2019  Sebastian Schüpbach <sebastian.schuepbach@unibas.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package utils

import com.typesafe.config.{Config, ConfigFactory}
import models.User
import pdi.jwt._
import play.api.mvc.Request

import scala.util.{Failure, Success}

object JwtServices {

  private val settings: Config = ConfigFactory.defaultApplication()

  def getCurrentUser[A](req: Request[A]): JWTParseResult = {
    val cookies = req.cookies
    val keycloakAccess = cookies.get("kc-access").flatMap(c => Some(c.value))
    keycloakAccess match {
      case Some(v) => Jwt.decode(v,
        settings.getString("keycloak.publicRS256Key"),
        Seq(JwtAlgorithm.RS256)) match {
        case Success(obj) =>
          User.createFromJWT(obj) match {
            case Some(u) => JWTParseSuccess(u)
            case None => NoUserInJWT("No user key found in JWT")
          }
        case Failure(ex) =>
          VerificationFailed("Verification failed")
      }
      case _ =>
        NoJWTFound("No JSON Web Token found")
    }

  }

}


sealed trait JWTParseResult

sealed abstract class JWTParseFailure(val ex: String) extends JWTParseResult

case class NoUserInJWT(override val ex: String) extends JWTParseFailure(ex)

case class VerificationFailed(override val ex: String) extends JWTParseFailure(ex)

case class NoJWTFound(override val ex: String) extends JWTParseFailure(ex)

case class JWTParseSuccess[A](res: A) extends JWTParseResult
