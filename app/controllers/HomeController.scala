/*
 * Copyright (C) 2019  Sebastian Schüpbach <sebastian.schuepbach@unibas.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package controllers

import com.typesafe.config.{Config, ConfigFactory}
import javax.inject._
import models.User
import play.api.mvc._
import utils.{DockerServices, JWTParseFailure, JWTParseSuccess, JwtServices}

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {

  private val settings: Config = ConfigFactory.defaultApplication()

  private val logger = play.api.Logger(this.getClass)

  /**
   * Create an Action to render an HTML page.
   *
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def index(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    JwtServices.getCurrentUser(request) match {
      case JWTParseSuccess(user: User) =>
        logger.logger.info(s"Granting access to user ${user.id}")
        Ok(views.html.index(user))
      case JWTParseSuccess(_) =>
        logger.logger.error(s"Username is not a string!")
        InternalServerError("Can't parse username")
      case f: JWTParseFailure =>
        logger.logger.warn(f.ex)
        BadRequest(f.ex)
    }
  }

  def logout(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    JwtServices.getCurrentUser(request) match {
      case JWTParseSuccess(user: User) =>
        logger.logger.info(s"User $user logs out")
        for (service <- DockerServices.getServices(user.id)) {
          logger.logger.info(s"Service with id ${service.getId} will be removed because user ${user.id} is logging out")
          DockerServices.removeService(service.getId)
        }
        Redirect(settings.getString("keycloak.logoutEndpoint")).discardingCookies(DiscardingCookie("kc-access"), DiscardingCookie("kc-state"))
      case JWTParseSuccess(_) =>
        logger.logger.error(s"User doesn't match model!")
        InternalServerError("Can't parse user")
      case f: JWTParseFailure =>
        logger.logger.warn(f.ex)
        BadRequest(f.ex)
    }
  }
}
