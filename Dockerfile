FROM hseeberger/scala-sbt:8u181_2.12.7_1.2.6 AS sbtsetup
WORKDIR /build
RUN sbt new scala/scala-seed.g8 --name=build -o . && rm -r src/test
COPY build.sbt .
COPY project/ ./project/
RUN sbt update

FROM hseeberger/scala-sbt:8u181_2.12.7_1.2.6 AS build
WORKDIR /build
COPY . .
COPY --from=sbtsetup /root/.ivy2/cache /root/.ivy2/cache
RUN sbt dist && unzip target/universal/shipyard-1.0-SNAPSHOT.zip

FROM openjdk:8-jre-slim-buster
EXPOSE 9000
ARG APP_SECRET
WORKDIR /app
ENTRYPOINT /run.sh "${APP_SECRET}"

COPY --from=build /build/shipyard-1.0-SNAPSHOT/* ./
COPY docker/run.sh /run.sh
RUN chmod +x /run.sh
