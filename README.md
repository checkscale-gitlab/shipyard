# shipyard

Frontend to manage personalised Docker swarm services

## Usage

```sh
APP_SECRET=`head -c32 /dev/urandom | base64`
docker run -d -p9000:9000 -eAPP_SECRET="$APP_SECRET" --rm --name shipyard shipyard
```

```sh
docker stop shipyard
```
